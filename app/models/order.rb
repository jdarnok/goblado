class Order < ActiveRecord::Base
  belongs_to :product
  belongs_to :user
  belongs_to :site
  self.per_page = 35
  def self.show_monthly(time, user)
    Order.includes(:site).where(:created_at => time.beginning_of_month..time.end_of_month, user: user)
  end
  def self.total_monthly(time, user)
    Order.includes(:site).where(:created_at => time.beginning_of_month..time.end_of_month, user: user).sum(:total)
  end



end
