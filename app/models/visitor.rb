class Visitor < ActiveRecord::Base
  belongs_to :site, :counter_cache => true
  scope :last_week, -> { where(created_at: 1.week.ago..DateTime.now.end_of_day) }
  scope :last_day, -> { where(:created_at => 1.day.ago..DateTime.now.end_of_day) }
  scope :last_month, ->  { where(:created_at => 1.month.ago..DateTime.now.end_of_day) }
end
