class Site < ActiveRecord::Base

  belongs_to :user
  belongs_to :product
  has_many :orders
  has_many :visitors
  self.per_page = 35
end
