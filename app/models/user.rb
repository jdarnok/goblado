class User < ActiveRecord::Base
  include Tokenable
  has_many :sites
  has_many :orders
  after_initialize :set_default_role, :if => :new_record?
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
  enum role: [ :admin, :partner ]

  def set_default_role
      self.role ||= :partner
  end

end
