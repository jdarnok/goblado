class DashboardController < ApplicationController
  before_action :authenticate_user!
  # TODO lokalizacja czasu i maile
  def index

    #Visitor.where(created_at: (Time.now - 1.day)..(Time.now))

    @sites = current_user.sites.includes(:product, :orders, :visitors).where(visitors: {uniq: true}).order(visitors_count: :desc).limit(10)
    @t1 = Time.now - 7.day
    @t2 = Time.now
    @orders = Order.show_monthly(@t2, current_user)
    @total = Order.total_monthly(@t2, current_user)
    if current_user.partner?
      @total = @total * 0.30
    end
    # days_number = Time.days_in_month(Date.today.month, Date.today.year)
    # @days = Array.new(days_number, 0 )
    # @sites.each do |site|
    #   visitors = site.visitors.last_month
    #   visitors.each do |visitor|
    #     @days[visitor.created_at.mday-1] += 1
    #   end
    # end
  end

  # def show_day
  #   @hours = Array.new(24, 0 )
  #   @sites = current_user.sites.includes(:product, :orders, :visitors)
  #   @sites.each do |site|
  #     visitors = site.visitors.last_day
  #     visitors.each do |visitor|
  #       @hours[visitor.created_at.localtime.hour] += 1
  #     end
  #   end
  #
  #   # respond_to do |format|
  #   #   format.js
  #   # end
  # end


  def show_custom_month
    Groupdate.time_zone = "Europe/Warsaw"
    @flag = false
    hour_flag = params[:hour_flag] == "on" ? true : false
    if params[:startdate].blank?
      @t1 = Time.now - 30.day
    else
      @t1 = Time.parse(params[:startdate])
    end
    if params[:enddate].blank?
      @t2 = Time.now
    else
      @t2 = Time.parse(params[:enddate])
    end
    diff = ((@t2 - @t1) / 1.hour).round
    if diff <= 48 || hour_flag
      @flag = true
    end
    if params[:site_url].blank?
      @sites = current_user.sites.includes(:product, :orders, :visitors).where(visitors: {created_at: @t1..@t2, uniq: true}).order(visitors_count: :desc).paginate(:page => params[:page])
    else
      @sites = current_user.sites.includes(:product, :orders, :visitors).where(visitors: {created_at: @t1..@t2, uniq: true}, id: params[:site_url]).paginate(:page => params[:page])
    end



  end
end
#Visitor.all.where(created_at: (Date.current - (Date.today - date).to_i.day)..Date.current)
#(Date.today - date).to_i
