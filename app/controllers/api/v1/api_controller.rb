class Api::V1::ApiController < ApplicationController
  skip_before_action :verify_authenticity_token
  respond_to :json

  def recieve
    ip = request.remote_ip

    if params[:uid].blank? || params[:uid] == "undefined" || User.find_by(token: params[:uid]).nil?
      user = User.first
    else
      user = User.find_by(token: params[:uid])
    end
    if params[:pid].blank? || params[:pid] == "undefined" || Product.find_by(token: params[:pid]).nil?
      product = Product.first
    else
      product = Product.find_by(token: params[:pid])
    end
    ref = params[:referal].blank? ? 'brak refa' : params[:referal]

    if ref.include? "http://www.google.com/"
      ref = 'google'
    end
    if ref.include? "http://www.google.pl/"
      ref = 'google'
    end

    site = Site.find_or_create_by(url: ref, user: user, product: product)
    #
    visitor = Visitor.find_or_initialize_by(ip: ip, site: site) do |visitor|
      visitor.created_at = (Time.zone.now.beginning_of_day..Time.zone.now.end_of_day)
    end
    if visitor.new_record?
      visitor.uniq = true
      visitor.save
    else
      Visitor.create(site: site, uniq: false, ip: ip)
      visitor.save
    end
    visitor.save
    # if uniq == 'false'
    # Visitor.where(created_at: (Time.now - 1.day)..(Time.now))
    #   value = site.nouniq + 1
    #   site.update_attribute(:nouniq, value)
    # else
    #   value = site.uniq + 1
    #   site.update_attribute(:uniq, value)
    #   value = site.nouniq + 1
    #   site.update_attribute(:nouniq, value)
    # end
    render json: { message: 'OK' }, status: 201

    # Order.create(site: site, product: product, user: user)
  end

  def recieve_order
    ip = request.remote_ip
    if params[:uid].blank? || params[:uid] == "undefined" || User.find_by(token: params[:uid]).nil?
      user = User.first
    else
      user = User.find_by(token: params[:uid])
    end
    if params[:pid].blank? || params[:pid] == "undefined" || Product.find_by(token: params[:pid]).nil?
      product = Product.first
    else
      product = Product.find_by(token: params[:pid])
    end
    ref = params[:referal].blank? ? 'brak refa' : params[:referal]
    site = Site.find_or_create_by(url: ref, user: user, product: product)
    description = params[:description].blank? ? 'brak' : params[:description]
    if params[:payment_type] == '1'
      payment = 'Płatność przy odbiorze'
    elsif params[:payment_type] == '2'
      payment = 'Płatność Online'
    else
      payment = 'Przelew tradycyjny'
    end
    order = Order.new(product: product, user: user, site: site, total: params[:total_price], payment_type: payment,
                      name: params[:name], surname: params[:surname], phone_number: params[:phone], zip: params[:zip],
                      city: params[:city], description: description, address: params[:address], ip: ip)
    if order.save!
      render json: { order_id: order.id }, status: 201
    else
      render json: { order_id: 0 }, status: 422
    end
  end
end
