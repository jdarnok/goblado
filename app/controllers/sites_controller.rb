class SitesController < ApplicationController
  before_action :authenticate_user!
  def index
    Groupdate.time_zone = 'Europe/Warsaw'

    @flag = false
    hour_flag = params[:hour_flag] == 'on' ? true : false
    if params[:startdate].blank?
      @t1 = Time.now - 30.day
    else
      @t1 = Time.parse(params[:startdate])
    end
    if params[:enddate].blank?
      @t2 = Time.now
    else
      @t2 = Time.parse(params[:enddate])
    end
    diff = ((@t2 - @t1) / 1.hour).round
    @flag = true if diff <= 48 || hour_flag
    if params[:all] == 'on' || params[:site_url].blank? || params[:site_url] == 'brak refa'
      @sites = current_user.sites.includes(:product, :orders, :visitors).where(visitors: { created_at: @t1..@t2, uniq: true }).order(visitors_count: :desc).paginate(page: params[:page])
    else
      @sites = current_user.sites.includes(:product, :orders, :visitors).where(visitors: { created_at: @t1..@t2, uniq: true }, id: params[:site_url]).paginate(page: params[:page])
    end
  end

  def show
  end

  def update
  end

  def create
  end

  def destroy
  end
end
