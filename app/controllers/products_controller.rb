class ProductsController < ApplicationController
  before_action :authenticate_user!
  def show
    @products = Product.find(params[:id])
  end
  def index
    @products = Product.all
  end
end
