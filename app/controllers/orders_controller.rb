# TODO widok zamowien, przyjrzec sie optymalizacji dashboardu
# TODO orders = user.orders.where(created_at: Time.now.beginning_of_month.. Time.now).group_by(&:created_at)
# days.each do |day, order|

class OrdersController < ApplicationController
  before_action :set_order, only: [:destroy,:show, :update, :create, :edit]
  before_action :authenticate_user!
  def index
    @orders = current_user.orders.includes(:product).order(:created_at).paginate(:page => params[:page])
  end

  def show
  end

  def show_monthly
    time = Time.parse(params[:date])
    @orders = current_user.orders.where(created_at: time.beginning_of_day.. time.end_of_day)
  end

  def summary

    if params[:time]

      @time = Time.parse(params[:time])
    else
      @time = Time.zone.now
    end
    @orders = current_user.orders
                  .where(created_at: @time.beginning_of_month .. @time.end_of_month)
                  .group('created_at::date')
                  .sum(:total)
                  .sort_by { |order| order}
    # @orders = Order.show_monthly(time, current_user).paginate(:page => params[:page])

  end

  def edit
  end

  def create
  end

  def destroy
    @order.destroy
    respond_to do |format|
      format.js
      format.html { redirect_to orders_url }
      format.json { head :no_content }
    end
  end

  def new
  end
end

private

def set_order
  @order = Order.find(params[:id])
end

# def order_params
#   params.require(:order).permit(:name, :description)
# end
