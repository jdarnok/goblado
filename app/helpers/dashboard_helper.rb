module DashboardHelper
  def wrap_text(txt, col = 80)
  txt.gsub(/(.{1,#{col}})(?: +|$\n?)|(.{1,#{col}})/,
    "\\1\\2\n")
  end
end
