Rails.application.routes.draw do
  get 'reports/show'

  get 'reports/index'

  require 'api_constraints'
  get 'products/show'

  root to: 'dashboard#index'
  # post 'api/recieve'
  # post 'api/recieve_order'
  get 'dashboard/index', as: 'dashboard'
  # post 'dashboard/show_with_timescale'
  # get 'dashboard/show_day', as: 'show_day'
  # get 'dashboard/show_week', as: 'show_week'
  # get 'dashboard/show_month', as: 'show_month'
  # get 'dashboard/show_custom_month'
  # post 'dashboard/show_custom_month'
  get 'orders/summary', as: "summary"
  get 'orders/show_monthly', as: "monthly_summary"
  resources :orders
  resources :products, only: [:show, :index]
  post 'sites/index', as: "custom_sites"
  get 'sites/index', as: "cutsom_sites"
  resources :sites
  devise_for :users
  resources :users


  namespace :api do
    scope module: :v1, costraints: ApiConstraints.new(version: 1, default: true) do
      post 'recieve', to: 'api#recieve'
      post 'recieve_order', to: 'api#recieve_order'
    end
  end

end
