class AddSurnameToOrders < ActiveRecord::Migration
  def change
    add_column :orders, :surname, :string
  end
end
