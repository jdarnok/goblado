class CreateSites < ActiveRecord::Migration
  def change
    create_table :sites do |t|
      t.string :url
      t.integer :uniq
      t.integer :nouniq
      t.references :order, index: true
      t.references :user, index: true

      t.timestamps null: false
    end
    add_foreign_key :sites, :orders
    add_foreign_key :sites, :users
  end
end
