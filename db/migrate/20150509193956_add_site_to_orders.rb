class AddSiteToOrders < ActiveRecord::Migration
  def change
    add_reference :orders, :site, index: true
    add_foreign_key :orders, :sites
  end
end
