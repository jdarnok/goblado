class AddProductToSites < ActiveRecord::Migration
  def change
    add_reference :sites, :product, index: true
    add_foreign_key :sites, :products
  end
end
