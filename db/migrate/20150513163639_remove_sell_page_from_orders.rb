class RemoveSellPageFromOrders < ActiveRecord::Migration
  def change
    remove_column :orders, :sell_page
  end
end
