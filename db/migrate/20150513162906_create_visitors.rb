class CreateVisitors < ActiveRecord::Migration
  def change
    create_table :visitors do |t|
      t.references :site, index: true
      t.boolean :uniq

      t.timestamps null: false
    end
    add_foreign_key :visitors, :sites
  end
end
